# bigdata_employment_visualization

#### 介绍
基于Hadoop+Spark的计算机类招聘职位大数据分析平台

#### 效果展示
首页：
![](./images/index.jpg)
表格页：
![](./images/table.jpg)

### 项目参考地址
参考网站：[计算机毕业设计Hadoop+Spark招聘推荐系统 招聘可视化-CSDN博客](https://blog.csdn.net/spark2022/article/details/140088344)

数据集：[计算机类人才招聘信息数据集_数据集-飞桨AI Studio星河社区 (baidu.com)](https://aistudio.baidu.com/datasetdetail/108664)

项目地址：https://gitee.com/waiZuiDragonKing-78/bigdata_employment_visualization.git

需求分析文档： https://docs.qq.com/doc/DT3lIV0hDQVl5RXpN

开发文档：[https://docs.qq.com/doc/DT0t2VHVZcGxSTFlw](https://gitee.com/link?target=https%3A%2F%2Fdocs.qq.com%2Fdoc%2FDT0t2VHVZcGxSTFlw)

#### 软件架构
软件架构说明


#### 目录说明

1.  bigdata_visualization_spring_boot：后端代码
2.  dataset：数据集
3.  nodeJsServer：静态资源部署服务器
4.  onddata：hive的建表语句
5.  web_source：前端Vue3代码
6.  images：图片资源

#### 安装教程

1.  git clone https://gitee.com/waiZuiDragonKing-78/bigdata_employment_visualization.git
2.  运行后端代码bigdata_visualization_spring_boot
3.  运行前端代码web_source